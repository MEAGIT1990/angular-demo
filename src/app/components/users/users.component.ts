import { Component, OnInit } from '@angular/core';
import {User} from './userModel';
import { RestClientService } from 'src/app/core/services/rest-client.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { flatten } from '@angular/compiler';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  title = 'appBootstrap';
  
  closeResult: string;
  users:User[] = [
    { id: 1, name: "Leanne Graham", username: "Bret",email: "mea@april.biz"},
    { id: 2, name: "Siaka", username: "Dembele",email: "skd@april.biz"}
  ]
  currentUser: any;
  lockFields: boolean;
  constructor(private restClient:RestClientService,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getUsersList()
  }
  
  getUsersList(){
    let endpoint = "users"
    this.restClient.getUsers(endpoint).subscribe(
      (response:any)=>{
        console.log('response',response);     
        this.users = response;
    },
    (err)=>{
      console.log('Erreur','Probleme communication server');
    },)
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


  showModal(content,user?:any,bool?:boolean){
    this.currentUser={}
    this.lockFields = user?bool:true;
    if(user){
      this.currentUser = {...user};
    }

    this.modalService.open(content);
  }
  saveUser(user:any){
    let endpoint = 'create';
    let data={
      name:user.name,
      username:user.username,
      email:user.email
    }
    this.restClient.createUsers(endpoint,data).subscribe(
      (response:any)=>{
        if(!response.hasError){
          console.log('succès',response.status.message);
        }
        else{
          console.log('error',response.status.message);
        }
      },
      (err:any)=>{
        console.log('error',err.status.message);
      }
    )

  }
  


}
