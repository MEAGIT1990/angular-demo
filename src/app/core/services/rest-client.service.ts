import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RestClientService {
  private baseUrl = "https://jsonplaceholder.typicode.com/";
  constructor(private http:HttpClient) { }

  getUsers(endPoint){
    return this.http.get(this.baseUrl+endPoint);

  }
  createUsers(endPoint,user:any){
    return this.http.post(this.baseUrl+endPoint,user);
  }
}
